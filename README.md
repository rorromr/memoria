# Memoria

## Versión compilada

La versión compilada en PDF puede descargarse desde [aquí](http://latex.aslushnikov.com/compile?git=git://github.com/rorromr/memoria&target=memoria.tex).

## Instalación en Ubuntu 14.04

Se deben instalar los siguiente paquetes para usar la plantilla usando Texmaker (4.5):

```
$ sudo apt-get install texlive-generic-extra texlive-fonts-recommended rubber
```

## Plantilla

Trabajo está basado en la [plantilla LaTeX](https://github.com/FCFM-ADI/memoria-latex) publicada por el ADI.
